﻿using ParkingApp.Interfaces;
using System;
using System.Linq;

namespace ParkingApp
{
    internal class ParkingMenu: IParkingMenu
    {
        private readonly IParking _parking;
        private readonly IOutputWriter _outputWriter;
        private readonly IInputReader _inputReader;
        public ParkingMenu (IParking parking, IOutputWriter outputWriter, IInputReader inputReader)
        {
            _parking = parking;
            _outputWriter = outputWriter;
            _inputReader = inputReader;
        }
        public bool ShowMenu()
        {
            PrintMainMenu();
            bool isMenuActive = MenuAction();

            return isMenuActive;
        }
        private void PrintMainMenu()
        {
            _outputWriter.WriteSecondaryMessage("[1] - Поставити транспортний засiб на парковку");
            _outputWriter.WriteSecondaryMessage("[2] - Забрати транспортний засiб з парковки");
            _outputWriter.WriteSecondaryMessage("[3] - Поповнити баланс транспортного засобу");
            _outputWriter.WriteSecondaryMessage("[4] - Вивести кiлькiсть вiльних/зайнятих мiсць на парковцi");
            _outputWriter.WriteSecondaryMessage("[5] - Вивести список усiх транспортних засобiв");
            _outputWriter.WriteSecondaryMessage("[6] - Вивести поточний баланс парковки");
            _outputWriter.WriteSecondaryMessage("[7] - Вивести прибуток парковки за останню хвилину");
            _outputWriter.WriteSecondaryMessage("[8] - Вивести усю iсторiю транзакцiй");
            _outputWriter.WriteSecondaryMessage("[С] - Очистити консоль");
            _outputWriter.WriteSecondaryMessage("[X] - Вийти");
        }

        private bool MenuAction()
        {
            _outputWriter.WriteMessage("Обрано пункт меню: ", true);
            var inputResult = _inputReader.Read();
            _outputWriter.WriteLineBreak();

            bool isMenuActive = true;

            switch (inputResult)
            {
                case InputResult.Quit:
                    isMenuActive = false;
                    break;
                case InputResult.Clear:
                    _outputWriter.Clear();
                    break;
                case InputResult.Item1:
                    AddVehicleToParking();
                    break;
                case InputResult.Item2:
                    RemoveVehicleFromParking();
                    break;
                case InputResult.Item3:
                    RechargeVehicleBalance();
                    break;
                case InputResult.Item4:
                    PrintAvailableParkingSpace();
                    break;
                case InputResult.Item5:
                    PrintListOfAllVehicles();
                    break;
                case InputResult.Item6:
                    PrintParkingBalance();
                    break;
                case InputResult.Item7:
                    PrintLastMinuteProfit();
                    break;
                case InputResult.Item8:
                    PrintAllTransactions();
                    break;
                default:
                    _outputWriter.WriteErrorMessage("Ви обрали неiснуючий пункт меню!");
                    break;
            }

            return isMenuActive;
        }

        private void AddVehicleToParking()
        {
            if (_parking.IsFull())
            {
                _outputWriter.WriteErrorMessage($"На жаль, всi мiсця вже зайнятi! Приїжджайте пiзнiше");
                return;
            }

            PrintAllVehicleTypes();

            _outputWriter.WriteMessage("Введiть номер типу транспортного засобу i натиснiть [ENTER]: ", true);
            bool isVehicleTypeNumberValid = Int32.TryParse(_inputReader.ReadMessage(), out int vehicleTypeNumber);
            //_outputWriter.WriteOutputLineBreak();

            bool isVehicleTypeNumberExists = IsVehicleTypeNumberExists(vehicleTypeNumber);

            if (isVehicleTypeNumberValid && isVehicleTypeNumberExists)
            {
                _outputWriter.WriteMessage("Введiть суму поповнення i натиснiть [ENTER]: ", true);
                bool isRechargeSumValid = Int32.TryParse(_inputReader.ReadMessage(), out int rechargeSum);
                //_outputWriter.WriteOutputLineBreak();

                if (isRechargeSumValid && rechargeSum > 0)
                {
                    Vehicle vehicle = new Vehicle((VehicleType)vehicleTypeNumber, rechargeSum);
                    _parking.AddVehicle(vehicle);

                    _outputWriter.WriteSuccessMessage($"Ваш транспортний заciб: {vehicle} успiшно припаркований");
                }
                else
                {
                    _outputWriter.WriteErrorMessage($"Введена недiйсна сума поповнення!");
                }
            }
            else
            {
                _outputWriter.WriteErrorMessage($"Введений недiйсний тип транспортного засобу!");
            }
        }

        private void PrintAllVehicleTypes()
        {
            foreach (var type in Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().OrderBy(t => t))
            {
                _outputWriter.WriteMessage($"{(int)type} - {type}");
            }
        }

        private bool IsVehicleTypeNumberExists(int vehicleTypeNumber)
        {
            return Enum.GetValues(typeof(VehicleType)).Cast<int>().Contains(vehicleTypeNumber);
        }

        private void RemoveVehicleFromParking()
        {
            _outputWriter.WriteMessage("Введiть ID транспортного засобу i натиснiть [ENTER]: ", true);
            bool isVehicleIdValid = Int32.TryParse(_inputReader.ReadMessage(), out int vehicleId);
            //_outputWriter.WriteOutputLineBreak();

            if (isVehicleIdValid && _parking.ContainsVehicle(vehicleId))
            {
                if (_parking.RemoveVehicle(vehicleId))
                {
                    _outputWriter.WriteSuccessMessage("Транспортний засiб успiшно забрано з парковки");
                }
                else
                {
                    _outputWriter.WriteErrorMessage("Забрати транспортний засiб неможливо - спочатку оплатiть штрафи!");
                    _outputWriter.WriteErrorMessage($"Сума штрафiв: {_parking.GetVehicleFineBalance(vehicleId)}");
                }
            }
            else
            {
                _outputWriter.WriteErrorMessage("Транспортного засобу з таким ID немає на парковцi! Мабуть Ви приїхали на таксi");
            }
        }

        private void RechargeVehicleBalance()
        {
            _outputWriter.WriteMessage("Введiть ID транспортного засобу i натиснiть [ENTER]: ", true);
            bool isVehicleIdValid = Int32.TryParse(_inputReader.ReadMessage(), out int vehicleId);
            //_outputWriter.WriteOutputLineBreak();

            if (isVehicleIdValid && _parking.ContainsVehicle(vehicleId))
            {
                _outputWriter.WriteMessage("Введiть суму поповнення i натиснiть [ENTER]: ", true);
                bool isRechargeSumValid = Int32.TryParse(_inputReader.ReadMessage(), out int rechargeSum);
                //_outputWriter.WriteOutputLineBreak();

                if (isRechargeSumValid && rechargeSum > 0)
                {
                    _parking.RechargeVehicleBalance(vehicleId, rechargeSum);
                    _outputWriter.WriteSuccessMessage($"Баланс транспортного засобу ID {vehicleId} поповнений на {rechargeSum}, на рахунку {_parking.GetVehicleBalance(vehicleId)}");
                }
                else
                {
                    _outputWriter.WriteErrorMessage("Введена недiйсна сума поповнення!");
                }
            }
            else
            {
                _outputWriter.WriteErrorMessage("Транспортного засобу з таким ID немає на парковцi! Мабуть Ви приїхали на таксi");
            }
        }

        private void PrintAvailableParkingSpace()
        {
            _outputWriter.WriteSuccessMessage($"Кiлькiсть вiльних мiсць на парковцi: {_parking.AvailableParkingSpace} iз {_parking.Capacity}");
        }

        private void PrintListOfAllVehicles()
        {
            var vehicles = _parking.GetAllVehicles().Select(v => v.ToString());

            if(vehicles.Any())
            {
                _outputWriter.WriteSuccessMessage("Список всiх транспортних засобiв на парковцi");
                _outputWriter.WriteMessage(String.Join(Environment.NewLine, vehicles));
            }
            else
            {
                _outputWriter.WriteSuccessMessage("Парковка порожня!");
            }
        }

        private void PrintParkingBalance()
        {
            _outputWriter.WriteSuccessMessage($"Загальний прибуток парковки: {_parking.Balance}");
        }

        private void PrintLastMinuteProfit()
        {
            var lastMinuteProfit = _parking.GetLastMinuteTransactions()
                                                 .Select(t => t.Payment)
                                                 .Sum();

            _outputWriter.WriteSuccessMessage($"Прибуток парковки за останню хвилину: {lastMinuteProfit}");
        }

        private void PrintAllTransactions()
        {
            var log = _parking.GetTransactionsLog();

            if (log.Any())
            {
                _outputWriter.WriteSuccessMessage("Список усіх транзакцій");
                _outputWriter.WriteMessage("Дата/час \t\t Сума транзакцiй");
                _outputWriter.WriteMessage(String.Join(Environment.NewLine, log));
            }
            else
            {
                _outputWriter.WriteSuccessMessage("Файл Transactions.log порожнiй!");
            }
        }
    }
}
