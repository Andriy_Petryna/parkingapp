﻿using ParkingApp.Interfaces;
using System;

namespace ParkingApp
{
    class InputReader : IInputReader
    {
        public InputResult? Read()
        {
            var pressedKey = Console.ReadKey();
            InputResult? inputResult = null;

            switch (pressedKey.Key)
            {
                case ConsoleKey.X:
                    inputResult = InputResult.Quit;
                    break;
                case ConsoleKey.C:
                    inputResult = InputResult.Clear;
                    break;
                case ConsoleKey.D1:
                case ConsoleKey.NumPad1:
                    inputResult = InputResult.Item1;
                    break;
                case ConsoleKey.D2:
                case ConsoleKey.NumPad2:
                    inputResult = InputResult.Item2;
                    break;
                case ConsoleKey.D3:
                case ConsoleKey.NumPad3:
                    inputResult = InputResult.Item3;
                    break;
                case ConsoleKey.D4:
                case ConsoleKey.NumPad4:
                    inputResult = InputResult.Item4;
                    break;
                case ConsoleKey.D5:
                case ConsoleKey.NumPad5:
                    inputResult = InputResult.Item5;
                    break;
                case ConsoleKey.D6:
                case ConsoleKey.NumPad6:
                    inputResult = InputResult.Item6;
                    break;
                case ConsoleKey.D7:
                case ConsoleKey.NumPad7:
                    inputResult = InputResult.Item7;
                    break;
                case ConsoleKey.D8:
                case ConsoleKey.NumPad8:
                    inputResult = InputResult.Item8;
                    break;
            }

            return inputResult;
        }

        public string ReadMessage()
        {
            return Console.ReadLine();
        }
    }
}
