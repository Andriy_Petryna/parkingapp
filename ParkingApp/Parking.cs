﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ParkingApp.Interfaces;

namespace ParkingApp
{
    internal class Parking : IParking, IDisposable
    {
        private const double LOG_TIMER_TIMEOUT_SECONDS = 60;

        private IParkingLogger _logger;

        private static Lazy<Parking> Lazy { get; } = new Lazy<Parking>(() => new Parking());
        private Timer PaymentTimer { get; }
        private Timer LogTimer { get; }
        private List<Vehicle> Vehicles { get; set; }
        private List<Transaction> Transactions { get; set; }

        public IParkingLogger Logger
        {
            private get { return _logger; }
            set { _logger = value ?? throw new NullReferenceException(); }
        }
        public static Parking Instance { get { return Lazy.Value; } }
        public decimal Balance { get; private set; }
        public int Capacity { get; }
        public int UnavailableParkingSpace { get { return Vehicles.Count; } }
        public int AvailableParkingSpace { get { return Capacity - UnavailableParkingSpace; } }
        
        private Parking()
        {
            Vehicles = new List<Vehicle>();
            Transactions = new List<Transaction>();

            Logger = new ParkingLogger(Settings.FileName);

            TimeSpan loggerPeriod = TimeSpan.FromSeconds(LOG_TIMER_TIMEOUT_SECONDS);
            LogTimer = new Timer(Log, null, loggerPeriod, loggerPeriod);

            TimeSpan paymentPeriod = TimeSpan.FromSeconds(Settings.Timeout);
            PaymentTimer = new Timer(Pay, null, paymentPeriod, paymentPeriod);

            Capacity = Settings.ParkingCapacity;
            Balance = Settings.ParkingBalance;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if(IsFull())
            {
                throw new InvalidOperationException("Can't add vehicle if parking is full");
            }

            Vehicles.Add(vehicle ?? throw new NullReferenceException());
        }
        public bool RemoveVehicle(int vehicleId)
        {
            Vehicle vehicle = GetVehicleById(vehicleId) ?? throw new NullReferenceException();

            if (vehicle.Balance >= 0)
            {
                Vehicles.Remove(vehicle);
                return true;
            }

            return false;
        }
        public void RechargeVehicleBalance(int vehicleId, decimal rechargeSum)
        {
            Vehicle vehicle = GetVehicleById(vehicleId) ?? throw new NullReferenceException();
            
            if(vehicle.Balance < 0)
            {
                // debit all fines from vehicle balance to parking balance
                decimal finesSum = -vehicle.Balance;
                Transactions.Add(new Transaction(DateTime.Now, vehicleId, finesSum));
                Balance += finesSum;
            }

            vehicle.RechargeBalance(rechargeSum);
        }
        public decimal GetLastMinuteEarnedMoney()
        {
            return Transactions.Sum(t => t.Payment);
        }
        public bool ContainsVehicle(int id)
        {
            return Vehicles.Where(c => c.Id == id).Count() != 0;
        }
        public decimal GetVehicleBalance(int vehicleId)
        {
            Vehicle vehicle = GetVehicleById(vehicleId) ?? throw new NullReferenceException();

            return vehicle.Balance;
        }
        public decimal GetVehicleFineBalance(int vehicleId)
        {
            decimal balance = GetVehicleBalance(vehicleId);

            return (balance < 0 ? -balance : 0);
        }
        public bool IsFull()
        {
            return AvailableParkingSpace == 0;
        }
        public IEnumerable<Vehicle> GetAllVehicles()
        {
            return Vehicles;
        }
        public IEnumerable<Transaction> GetLastMinuteTransactions()
        {
            return Transactions;
        }
        public IEnumerable<string> GetTransactionsLog()
        {
            return Logger.ReadLogFromFile()
                         .Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }
        private void Log(object obj)
        {
            Logger.LogTransactionsSumToFile(GetLastMinuteEarnedMoney());
            Transactions.Clear();
        }
        private void Pay(object obj)
        {
            foreach (Vehicle vehicle in Vehicles)
            {
                decimal price = Settings.GetPrice(vehicle.Type);

                if (price <= vehicle.Balance)
                {
                    vehicle.WithdrawBalance(price);
                    Balance += price;

                    Transactions.Add(new Transaction(DateTime.Now, vehicle.Id, price));
                }
                else
                {
                    vehicle.WithdrawBalance(price * Settings.FineCoefficient);
                }
            }
        }
        private Vehicle GetVehicleById(int vehicleId)
        {
            return Vehicles.Where(c => c.Id == vehicleId)
                           .FirstOrDefault();
        }
        public void Dispose()
        {
            LogTimer.Dispose();
            PaymentTimer.Dispose();
        }
    }
}
