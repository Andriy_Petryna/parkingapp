﻿using System;
using System.Threading;

namespace ParkingApp
{
    internal enum VehicleType
    {
        Passenger,
        Truck,
        Bus,
        Motorcyle
    }
    internal class Vehicle
    {
        private static int idCounter = 0;

        public int Id { get; private set; }
        public decimal Balance { get; private set; }
        public VehicleType Type { get; private set; }

        public Vehicle(VehicleType type) : this(type, 0) 
        {}
        public Vehicle(VehicleType type, decimal balance)
        {
            Type = type;
            Balance = balance;
            Id = Interlocked.Increment(ref idCounter);
        }

        public void WithdrawBalance(decimal amount)
        {
            if(amount < 0)
            {
                throw new ArgumentException("Negative withdraw amount");
            }

            Balance -= amount;
        }
        public void RechargeBalance(decimal amount)
        {
            if (amount < 0)
            {
                throw new ArgumentException("Negative recharge amount");
            }

            Balance += amount;
        }
        public override string ToString()
        {
            return $"ID: {Id}; Тип: {Type}; Баланс: {Balance};";
        }
    }
}
