﻿using ParkingApp.Interfaces;
using System;

namespace ParkingApp
{
    class OutputWriter: IOutputWriter
    {
        public void WriteLineBreak()
        {
            Console.WriteLine();
        }
        public void WriteMessage(string message, bool withoutLineBreak = false)
        {
            if(withoutLineBreak)
            {
                PrintLineWithoutBreak(message);
            }
            else
            {
                PrintFullWidthLine(message);
            }
        }

        public void WriteSecondaryMessage(string message)
        {
            SetGrayLineColor();
            PrintFullWidthLine(message);
            ResetLineColor();
        }

        public void WriteSuccessMessage(string message)
        {
            SetGreenLineColor();
            PrintFullWidthLine(message);
            ResetLineColor();
        }

        public void WriteErrorMessage(string message)
        {
            SetRedLineColor();
            PrintFullWidthLine(message);
            ResetLineColor();
        }

        public void Clear()
        {
            Console.Clear();
        }

        private void PrintLineWithoutBreak(string message)
        {
            Console.Write(message);
        }

        private void PrintFullWidthLine(string message)
        {
            Console.WriteLine(message.PadRight(Console.WindowWidth - 1));
        }

        private void SetGrayLineColor()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.Gray;
        }

        private void SetRedLineColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Red;
        }

        private void SetGreenLineColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkGreen;
        }

        private void ResetLineColor()
        {
            Console.ResetColor();
        }
    }
}
