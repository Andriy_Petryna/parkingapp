﻿namespace ParkingApp.Interfaces
{
    enum InputResult
    {
        Item1,
        Item2,
        Item3,
        Item4,
        Item5,
        Item6,
        Item7,
        Item8,
        Quit,
        Clear,
    }
    interface IInputReader
    {
        InputResult? Read();
        string ReadMessage();
        
    }
}
