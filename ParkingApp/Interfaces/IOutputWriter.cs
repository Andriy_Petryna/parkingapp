﻿namespace ParkingApp.Interfaces
{
    internal interface IOutputWriter
    {
        void WriteLineBreak();
        void WriteMessage(string message, bool withoutLineBreak = false);
        void WriteSecondaryMessage(string message);
        void WriteSuccessMessage(string message);
        void WriteErrorMessage(string message);
        void Clear();
    }
}
