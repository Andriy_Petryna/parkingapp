﻿namespace ParkingApp.Interfaces
{
    internal interface IParkingMenu
    {
        bool ShowMenu();
    }
}
