﻿using System.Collections.Generic;

namespace ParkingApp.Interfaces
{
    interface IParking
    {
        decimal Balance { get; }
        int AvailableParkingSpace { get; }
        int Capacity { get; }

        void AddVehicle(Vehicle vehicle);
        bool RemoveVehicle(int vehicleId);
        void RechargeVehicleBalance(int vehicleId, decimal rechargeSum);
        decimal GetLastMinuteEarnedMoney();
        bool ContainsVehicle(int id);
        decimal GetVehicleBalance(int vehicleId);
        decimal GetVehicleFineBalance(int vehicleId);
        bool IsFull();
        IEnumerable<Vehicle> GetAllVehicles();
        IEnumerable<Transaction> GetLastMinuteTransactions();
        IEnumerable<string> GetTransactionsLog();
    }
}
