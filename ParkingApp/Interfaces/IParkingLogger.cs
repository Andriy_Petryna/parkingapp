﻿namespace ParkingApp.Interfaces
{
    interface IParkingLogger
    {
        void LogTransactionsSumToFile(decimal sum);
        string ReadLogFromFile();
    }
}
