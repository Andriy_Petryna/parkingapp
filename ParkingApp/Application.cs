﻿using ParkingApp.Interfaces;
using System;

namespace ParkingApp
{
    internal class Application
    {
        private readonly IParkingMenu _menu;
        private readonly IOutputWriter _outputWriter;

        public Application(IParkingMenu menu, IOutputWriter outputWriter)
        {
            _menu = menu;
            _outputWriter = outputWriter;
        }
        public void Run()
        {
            bool isMenuActive = true;
            do
            {
                try
                {
                    isMenuActive = _menu.ShowMenu();
                }
                catch (Exception ex)
                {
                    _outputWriter.WriteErrorMessage($"Сталася помилка: {ex.Message}");
                }
            } while (isMenuActive);
        }
    }
}
