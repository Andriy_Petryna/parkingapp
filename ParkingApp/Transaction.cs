﻿using System;

namespace ParkingApp
{
    internal class Transaction
    {
        public DateTime Time { get; }
        public int VehicleId { get; }
        public decimal Payment { get; }
        public Transaction(DateTime time, int vehicleId, decimal payment)
        {
            Time = time;
            VehicleId = vehicleId;
            Payment = payment; 
        }
        public override string ToString()
        {
            return $"Час: {Time}; ID транспортного засобу: {VehicleId}; Списані кошти: {Payment};";
        }
    }
}
