﻿using Autofac;
using ParkingApp.Interfaces;

namespace ParkingApp
{
    class Program
    {
        static private IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>();
            builder.RegisterType<ParkingMenu>()
                   .As<IParkingMenu>();
            builder.RegisterType<OutputWriter>()
                   .SingleInstance()
                   .As<IOutputWriter>();
            builder.RegisterType<InputReader>()
                    .SingleInstance()
                    .As<IInputReader>();
            builder.Register(p => Parking.Instance)
                   .SingleInstance()
                   .As<IParking>();
            return builder.Build();
        }
        static void Main(string[] args)
        {
            CompositionRoot().Resolve<Application>()
                             .Run();
        }
    }
}
