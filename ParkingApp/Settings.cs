﻿using System;
using System.Collections.Generic;

namespace ParkingApp
{
    internal static class Settings
    {
        private const decimal DEFAULT_PARKING_BALANCE = 0;
        private const int DEFAULT_PARKING_CAPACITY = 10;
        private const int DEFAULT_TIMEOUT_SECONDS = 5;
        private const decimal DEFAULT_FINE_COEFFICIENT = 2.5M;

        private const decimal DEFAULT_PRICE_PASSENGER = 2;
        private const decimal DEFAULT_PRICE_TRUCK = 5;
        private const decimal DEFAULT_PRICE_BUS = 3.5M;
        private const decimal DEFAULT_PRICE_MOTORCYCLE = 1;

        private const string DEFAULT_LOG_FILE_NAME = "Transactions.log";

        public static int Timeout { get; }
        public static Dictionary<VehicleType, decimal> Prices { get; }
        public static int ParkingCapacity { get; }
        public static decimal ParkingBalance { get; }
        public static decimal FineCoefficient { get; }
        public static string FileName { get; }

        static Settings()
        { 
            Timeout = DEFAULT_TIMEOUT_SECONDS;
            Prices = new Dictionary<VehicleType, decimal>()
            {
                { VehicleType.Truck, DEFAULT_PRICE_TRUCK },
                { VehicleType.Passenger, DEFAULT_PRICE_PASSENGER},
                { VehicleType.Bus, DEFAULT_PRICE_BUS },
                { VehicleType.Motorcyle, DEFAULT_PRICE_MOTORCYCLE }
            };
            ParkingCapacity = DEFAULT_PARKING_CAPACITY;
            ParkingBalance = DEFAULT_PARKING_BALANCE;
            FineCoefficient = DEFAULT_FINE_COEFFICIENT;
            FileName = DEFAULT_LOG_FILE_NAME;
        }

        public static decimal GetPrice(VehicleType type)
        {
            if (Prices.TryGetValue(type, out decimal price))
            {
                return price;
            }

            throw new ArgumentException($"Price for VehicleType.{type} is not setted");
        }
    }
}
